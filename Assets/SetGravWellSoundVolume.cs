﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGravWellSoundVolume : MonoBehaviour
{
    private const float maxVolume = 0.4f;
    private const float minVolume = 0f;
    private const float distWhereVolumeIsMax = 8f;
    private const float fadeDistance = 6f;

    AudioSource gravWellSound;

    private void Start()
    {
        gravWellSound = GetComponent<AudioSource>();
    }

    void Update()
    {
        if(gravWellSound != null)
            gravWellSound.volume = CalculateVolume();
    }

    private float CalculateVolume()
    {
        float dist = Vector2.Distance(transform.position, CameraFollow.Instance.transform.position);
        if (dist > distWhereVolumeIsMax + fadeDistance)
            return 0f;
        else if (dist <= distWhereVolumeIsMax)
            return maxVolume;
        else
            return Mathf.Lerp(minVolume, maxVolume, (dist - distWhereVolumeIsMax) / fadeDistance);
    }
}
