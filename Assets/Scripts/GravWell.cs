﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravWell : MonoBehaviour
{
    private const float speed = 40f;

    public enum Direction
    {
        North,
        South,
        East,
        NorthEast,
        SouthEast,
        West,
        NorthWest,
        SouthWest
    }

    private static Dictionary<Direction, Vector2> dirToVector = new Dictionary<Direction, Vector2>() {
        { Direction.North, Vector2.up },
        { Direction.South, Vector2.down },
        { Direction.East, Vector2.right },
        { Direction.West, Vector2.left },
        { Direction.NorthEast, Vector2.right + Vector2.up},
        { Direction.SouthEast, Vector2.right + Vector2.down},
        { Direction.NorthWest, Vector2.left + Vector2.up},
        { Direction.SouthWest, Vector2.left + Vector2.down},
    };

    public Direction direction;

    private GravWellGroup gravWellGroup;
    private GameObject occupant;

    public GameObject Occupant
    {
        get
        {
            return occupant;
        }
    }

    private void Awake()
    {
        gravWellGroup = GetComponentInParent<GravWellGroup>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character") && gravWellGroup.CanEnter(collision.gameObject))
        {
            occupant = collision.gameObject;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject == occupant)
        {
            Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
            rb.velocity = dirToVector[direction] * speed;
            rb.gravityScale = 0f;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == occupant)
        {
            Rigidbody2D rb = collision.GetComponent<Rigidbody2D>();
            rb.gravityScale = 8f;
            occupant = null;
        }
    }
}
