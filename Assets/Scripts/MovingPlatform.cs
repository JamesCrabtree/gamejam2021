﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    private const float waitTime = 0.75f;

    public float speed = 5f;
    public List<Transform> points;

    private Transform currPoint;
    private int currPointIndex;

    private Rigidbody2D rb;

    private bool waiting;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        if(points.Count > 0)
            currPoint = points[0];
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!waiting)
        {
            if (Vector2.Distance(transform.position, currPoint.position) < 0.2f)
            {
                StartCoroutine(waitAtPoint());
            }
            else
            {
                Vector2 dir = currPoint.position - transform.position;
                rb.velocity = dir.normalized * speed;
            }
        }
    }

    private IEnumerator waitAtPoint()
    {
        waiting = true;
        rb.velocity = Vector2.zero;
        yield return new WaitForSeconds(waitTime);
        currPointIndex = currPointIndex + 1 < points.Count ? currPointIndex + 1 : 0;
        currPoint = points[currPointIndex];
        waiting = false;
    }
}
