﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorState : MonoBehaviour
{
    public enum Colors
    {
        Neutral,
        Red,
        Blue
    }

    public Colors StartingColor;
    private void Awake()
    {
        myColor = StartingColor;
    }

    private Colors myColor;
    public Colors MyColor
    {
        get
        {
            return myColor;
        }
    }

    public void FlipColor()
    {
        if (myColor == Colors.Red)
            myColor = Colors.Blue;
        else if (myColor == Colors.Blue)
            myColor = Colors.Red;
    }

    public bool Blue()
    {
        return myColor == Colors.Blue;
    }

    public bool Red()
    {
        return myColor == Colors.Red;
    }
}
