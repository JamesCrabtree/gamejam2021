﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    private bool prevBlueInThisMagnet, prevRedInThisMagnet;

    private PlayerReference bluePlayer, redPlayer;
    private PlayerMagnetState bluePlayerMagnetState, redPlayerMagnetState;
    private ColorState bluePlayerColorState, redPlayerColorState, myColorState;
    private Rigidbody2D blueRB, redRB;
    private AudioSource magnetSound;
    private bool thisMagnetInUse;

    private const float magnetRange = 4f;
    private const float maxSpeed = 30f;

    void Start()
    {
        bluePlayer = PlayerReference.BlueInstance;
        redPlayer = PlayerReference.RedInstance;
        bluePlayerColorState = bluePlayer.GetComponent<ColorState>();
        redPlayerColorState = redPlayer.GetComponent<ColorState>();
        bluePlayerMagnetState = bluePlayer.GetComponent<PlayerMagnetState>();
        redPlayerMagnetState = redPlayer.GetComponent<PlayerMagnetState>();
        blueRB = bluePlayer.GetComponent<Rigidbody2D>();
        redRB = redPlayer.GetComponent<Rigidbody2D>();
        myColorState = GetComponent<ColorState>();
        magnetSound = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        bool previouslyThisMagnetInUse = thisMagnetInUse;
        thisMagnetInUse = MagnetRedPlayer() || MagnetBluePlayer();
        if (thisMagnetInUse && !previouslyThisMagnetInUse && magnetSound != null)
        {
            magnetSound.volume = Random.Range(0.9f, 1f);
            magnetSound.pitch = Random.Range(0.9f, 1.1f);
            magnetSound.Play();
        }

        if (MagnetBluePlayer())
        {
            bluePlayerMagnetState.InAMagnet = true;
            prevBlueInThisMagnet = true;
            Vector2 dif = transform.position - bluePlayer.transform.position;
            //blueRB.velocity = Mathf.Min(maxSpeed, dif.magnitude) * dif.normalized;

            blueRB.velocity = Vector2.zero;
            bluePlayer.transform.position = Vector2.MoveTowards(bluePlayer.transform.position, transform.position, maxSpeed * Time.fixedDeltaTime);
        }
        else if (prevBlueInThisMagnet)
        {
            bluePlayerMagnetState.InAMagnet = false;
            prevBlueInThisMagnet = false;
        }

        if (MagnetRedPlayer())
        {
            redPlayerMagnetState.InAMagnet = true;
            prevRedInThisMagnet = true;
            Vector2 dif = transform.position - redPlayer.transform.position;
            //redRB.velocity = Mathf.Min(maxSpeed, dif.magnitude) * dif.normalized;

            redRB.velocity = Vector2.zero;
            redPlayer.transform.position = Vector2.MoveTowards(redPlayer.transform.position, transform.position, maxSpeed * Time.fixedDeltaTime);
        }
        else if (prevRedInThisMagnet)
        {
            redPlayerMagnetState.InAMagnet = false;
            prevRedInThisMagnet = false;
        }
    }

    private bool MagnetBluePlayer()
    {
        return myColorState.MyColor == bluePlayerColorState.MyColor &&
            Vector2.Distance(bluePlayer.transform.position, transform.position) < magnetRange
            && LineOfSight.InClearLineOfSight(bluePlayer.transform.position, transform.position);
    }

    private bool MagnetRedPlayer()
    {
        return myColorState.MyColor == redPlayerColorState.MyColor &&
            Vector2.Distance(redPlayer.transform.position, transform.position) < magnetRange
            && LineOfSight.InClearLineOfSight(redPlayer.transform.position, transform.position);
    }
}
