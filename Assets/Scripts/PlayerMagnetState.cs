﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMagnetState : MonoBehaviour
{
    private bool inAMagnet;
    public bool InAMagnet
    {
        get
        {
            return inAMagnet;
        }
        set
        {
            inAMagnet = value;
        }
    }

    void Start()
    {
        inAMagnet = false;
    }
}
