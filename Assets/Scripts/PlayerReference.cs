﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReference : MonoBehaviour
{
    public static PlayerReference RedInstance;
    public static PlayerReference BlueInstance;

    void Awake()
    {
        ColorState colorScript = GetComponent<ColorState>();
        if (colorScript.StartingColor == ColorState.Colors.Red)
            RedInstance = this;
        else
            BlueInstance = this;

        reachedGoal = false;
    }

    public PlayerReference GetOtherPlayerReference()
    {
        if (this == RedInstance)
            return BlueInstance;
        else
            return RedInstance;
    }

    private bool reachedGoal;
    public bool ReachedGoal
    {
        get
        {
            return reachedGoal;
        }
        set
        {
            reachedGoal = value;
        }
    }
}
