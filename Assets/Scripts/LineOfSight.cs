﻿using UnityEngine;
using System.Collections;

public static class LineOfSight
{
    private const int groundLayerMap = (1 << 8);

    public static bool InClearLineOfSight(Vector2 a, Vector2 b)
    {
        Vector2 direction = (b - a).normalized;
        int layerMask = groundLayerMap;
        float fullDist = Vector2.Distance(a, b);
        RaycastHit2D[] allSight = Physics2D.RaycastAll(a, direction, fullDist, layerMask);

        foreach (RaycastHit2D sight in allSight)
        {
            if (sight.collider.transform.tag == "Ground")
                return false;
        }
        return true;
    }
}
