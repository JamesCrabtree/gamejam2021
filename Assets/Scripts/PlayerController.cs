﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private const float speed = 15;
    private const float jumpSpeed = 30;

    private Rigidbody2D rb;
    private IsNearPlatform nearPlatformScript;
    private PlayerMagnetState playerMagnetState, otherPlayerMagnetState;
    private PlayerReference thisPlayer, otherPlayer;
    private AudioSource landSound, jumpSound, colorSwitchSound;

    private bool onGroundBefore;
    private float timeLeftGround;

    private bool jump;

    void Start()
    {
        thisPlayer = GetComponent<PlayerReference>();
        otherPlayer = thisPlayer.GetOtherPlayerReference();
        rb = GetComponent<Rigidbody2D>();
        nearPlatformScript = GetComponentInChildren<IsNearPlatform>();
        playerMagnetState = GetComponent<PlayerMagnetState>();
        if(otherPlayer != null)
            otherPlayerMagnetState = otherPlayer.GetComponent<PlayerMagnetState>();

        Transform landSoundObject = transform.Find("Land Sound");
        if (landSoundObject != null)
            landSound = landSoundObject.GetComponent<AudioSource>();

        Transform jumpSoundObject = transform.Find("Jump Sound");
        if (jumpSoundObject != null)
            jumpSound = jumpSoundObject.GetComponent<AudioSource>();

        Transform colorSwitchSoundObject = transform.Find("Color Switch Sound");
        if (colorSwitchSoundObject != null)
            colorSwitchSound = colorSwitchSoundObject.GetComponent<AudioSource>();

        onGroundBefore = nearPlatformScript != null && rb != null && nearPlatformScript.NearPlatform;
        timeLeftGround = Time.time;
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            jump = true;
        }
        if (Input.GetKeyDown("r"))
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void FixedUpdate()
    {
        if (onGroundBefore && nearPlatformScript != null && rb != null && !nearPlatformScript.NearPlatform)
        {
            timeLeftGround = Time.fixedTime;
        }

        if (nearPlatformScript != null && rb != null && nearPlatformScript.NearPlatform && !onGroundBefore)
        {
            float magnitudeMultiplier = Mathf.Clamp((Time.fixedTime - timeLeftGround) / 0.7f, 0.5f, 4f);
            float durationMultiplier = Mathf.Lerp(1f, 4f, (Time.fixedTime - timeLeftGround) / 3f);
            CameraShake.Instance.Shake(magnitudeMultiplier, durationMultiplier);
            if (landSound != null)
            {
                landSound.volume = Mathf.Lerp(0.4f, 0.7f, (Time.fixedTime - timeLeftGround) / 2.2f) + Random.Range(0.03f, -0.03f);
                landSound.pitch = Mathf.Lerp(0.8f, 0.56f, (Time.fixedTime - timeLeftGround) / 2.2f) + Random.Range(0.03f, -0.03f);
                landSound.Play();
            }
        }

        onGroundBefore = nearPlatformScript != null && rb != null && nearPlatformScript.NearPlatform;

        float h = Input.GetAxis("Horizontal");
        float v = jump ? jumpSpeed : 0;

        if (jumpSound != null && jump)
        {
            jumpSound.volume = Random.Range(0.25f, 0.35f);
            jumpSound.pitch = Random.Range(0.7f, 0.8f);
            jumpSound.Play();
        }
        jump = false;

        if (canMove() && Mathf.Abs(h + v + nearPlatformScript.platformVelocity.x) > 0f)
        {
            float speedModifier = otherPlayerMagnetState != null && !playerMagnetState.InAMagnet && otherPlayerMagnetState.InAMagnet ? 2f : 1f;
            rb.velocity = new Vector2(nearPlatformScript.platformVelocity.x + h * speed * speedModifier, rb.velocity.y + v);
        }
    }

    private bool canMove()
    {
        return nearPlatformScript != null && rb != null && playerMagnetState != null &&
            ((nearPlatformScript.NearPlatform && !playerMagnetState.InAMagnet) ||
            (otherPlayerMagnetState != null && !playerMagnetState.InAMagnet && otherPlayerMagnetState.InAMagnet));
    }

    public void PlayColorSwitchSound()
    {
        if (colorSwitchSound != null)
        {
            colorSwitchSound.volume = Random.Range(0.8f, 1f);
            colorSwitchSound.pitch = Random.Range(0.8f, 1f);
            colorSwitchSound.Play();
        }
    }
}
