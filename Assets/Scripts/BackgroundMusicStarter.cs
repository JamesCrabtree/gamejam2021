﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusicStarter : MonoBehaviour
{
    AudioSource[] musicList;
    public static bool playingMusic;
    private const float soundVolume = 0.75f;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (playingMusic)
            Destroy(this.gameObject);
    }

    private void Start()
    {
        musicList = GetComponents<AudioSource>();
        StartCoroutine(playAllMusic());
    }

    IEnumerator playAllMusic()
    {
        playingMusic = true;
        while (true)
        {
            foreach (AudioSource music in musicList)
            {
                if (music == null)
                    continue;

                music.volume = soundVolume;
                music.Play();
                yield return new WaitUntil(() => music.isPlaying);
                yield return new WaitUntil(() => !music.isPlaying);
                music.Stop();
                yield return new WaitForSeconds(2f);
            }
            yield return null;
        }
    }

}
