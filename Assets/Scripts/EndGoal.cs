﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGoal : MonoBehaviour
{
    private ColorState myColorState;
    public static bool BlueGoalExists;
    public static bool RedGoalExists;

    private void Awake()
    {
        myColorState = GetComponent<ColorState>();
        BlueGoalExists = false;
        RedGoalExists = false;
    }

    private void Start()
    {
        if (myColorState.StartingColor == ColorState.Colors.Blue)
            BlueGoalExists = true;
        else if (myColorState.StartingColor == ColorState.Colors.Red)
            RedGoalExists = true;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Character"))
        {
            ColorState colorState = collider.GetComponent<ColorState>();
            PlayerReference playerReference = collider.GetComponent<PlayerReference>();
            if (colorState != null && playerReference != null && 
                (colorState.MyColor == ColorState.Colors.Neutral || myColorState.MyColor == colorState.MyColor))
            {
                playerReference.ReachedGoal = true;

                PlayerReference otherPlayerReference = playerReference.GetOtherPlayerReference();

                if (otherPlayerReference != null && otherPlayerReference.ReachedGoal)
                    LoadScene();
                else if (otherPlayerReference == null && (CameraFollow.Instance.onlyRed || CameraFollow.Instance.onlyBlue))
                    LoadScene();
                else if (otherPlayerReference != null && ((BlueGoalExists && !RedGoalExists) || (!BlueGoalExists && RedGoalExists)))
                    LoadScene();
            }
        }
    }

    void LoadScene()
    {
        Debug.Log("SUCCESS!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
