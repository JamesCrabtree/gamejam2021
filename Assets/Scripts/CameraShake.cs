﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{

    bool shaking;
    bool startingShake;
    bool waiting;
    private const float startingMagnitude = 0.009f;

    private const float shakeDuration = 0.18f;

    Coroutine savedCoroutine;

    public static CameraShake Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        waiting = false;
        shaking = false;
    }

    public void Shake(float multiplier = 1f, float durationMultiplier = 1f)
    {
        if (multiplier <= 0f)
            return;

        startingShake = true;
        if (savedCoroutine != null)
            StopCoroutine(savedCoroutine);
        savedCoroutine = StartCoroutine(shakeInternal(multiplier, durationMultiplier));
        shaking = true;
        startingShake = false;
    }

    public bool Shaking
    {
        get { return shaking || startingShake; }
    }

    private float magnitudeOverTime(float elapsed, float dur, float max)
    {
        return max * Mathf.Pow(1f - (elapsed / dur), 0.7f);
        //return max;
    }

    private IEnumerator shakeInternal(float multiplier, float durationMultiplier)
    {
        float startTime = Time.time;
        float duration = shakeDuration * durationMultiplier;
        float m = startingMagnitude;
        float y = Random.Range(-1f, 1f) * m;
        float x = Random.Range(-1f, 1f) * m;
        Vector3 origPosition = transform.position;
        bool starting = true;
        Vector3 shakeOffset = Vector3.zero;
        Quaternion targetRotation = Quaternion.identity;
        while (Time.time - startTime < duration && !waiting)
        {
            shakeOffset = new Vector2(x, y);
            float xNeg = x / Mathf.Abs(x);
            float yNeg = y / Mathf.Abs(y);
            m = magnitudeOverTime(Time.time - startTime, duration, startingMagnitude) * multiplier;
            x = xNeg;
            y = yNeg;
            shakeOffset = Random.insideUnitCircle * new Vector2(x, y) * m;
            if (shakeOffset.magnitude > 0f)
                transform.rotation = Quaternion.LookRotation(Vector3.forward + shakeOffset);
            else
                transform.rotation = Quaternion.Euler(Vector3.zero);
            yield return new WaitForEndOfFrame();
        }

        transform.rotation = Quaternion.LookRotation(Vector3.forward);

        shaking = false;
    }
}
