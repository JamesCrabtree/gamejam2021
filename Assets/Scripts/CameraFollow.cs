﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform bluePos;
    private Transform redPos;

    public bool onlyRed;
    public bool onlyBlue;

    public static CameraFollow Instance;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if (!onlyRed)
            bluePos = PlayerReference.BlueInstance.transform;
        if(!onlyBlue)
            redPos = PlayerReference.RedInstance.transform;
    }

    void LateUpdate()
    {
        if(onlyRed)
        {
            if(redPos != null)
                transform.position = new Vector3(redPos.position.x, redPos.position.y, transform.position.z);
        }
        else if(onlyBlue)
        {
            if(bluePos != null)
                transform.position = new Vector3(bluePos.position.x, bluePos.position.y, transform.position.z);
        }
        else if (redPos != null && bluePos != null)
        {
            Vector2 middlePos = (bluePos.position + redPos.position) / 2f;
            transform.position = new Vector3(middlePos.x, middlePos.y, transform.position.z);
        }
    }
}
