﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FailOnOppositeColorCollision : MonoBehaviour
{
    private ColorState myColorState;

    private void Awake()
    {
        myColorState = GetComponent<ColorState>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ColorState colorState = collision.collider.GetComponent<ColorState>();
        if (colorState != null && colorState.MyColor != ColorState.Colors.Neutral && myColorState.MyColor != colorState.MyColor)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
