﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsNearPlatform : MonoBehaviour
{
    private bool isNearPlatform;
    private Rigidbody2D platformRb;

    public bool NearPlatform
    {
        get
        {
            return isNearPlatform;
        }
    }

    public Vector2 platformVelocity
    {
        get
        {
            if (platformRb != null)
                return platformRb.velocity;
            return Vector2.zero;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            isNearPlatform = true;
            platformRb = collision.GetComponent<Rigidbody2D>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground"))
        {
            isNearPlatform = false;
            platformRb = null;
        }
    }
}
