﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSwitch : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            Transform blueTransform = PlayerReference.BlueInstance.transform;
            Transform redTransform = PlayerReference.RedInstance.transform;

            Rigidbody2D blueRb = PlayerReference.BlueInstance.GetComponent<Rigidbody2D>();
            Rigidbody2D redRb = PlayerReference.RedInstance.GetComponent<Rigidbody2D>();

            Vector2 tempVelocity = blueRb.velocity;
            blueRb.velocity = redRb.velocity;
            redRb.velocity = tempVelocity;

            Vector2 tempPosition = blueTransform.position;
            blueTransform.position = redTransform.position;
            redTransform.position = tempPosition;

            if (blueTransform != null)
            {
                PlayerController playerController = blueTransform.GetComponent<PlayerController>();
                if (playerController != null)
                    playerController.PlayColorSwitchSound();
            }

            Destroy(gameObject);
        }
    }
}
