﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GravWellGroup : MonoBehaviour
{
    private List<GravWell> gravWells;

    private void Awake()
    {
        gravWells = GetComponentsInChildren<GravWell>().ToList();
    }

    private GameObject getCurrentOccupant()
    {
        foreach (GravWell gravWell in gravWells)
        {
            if (gravWell.Occupant != null)
                return gravWell.Occupant;
        }
        return null;
    }

    public bool CanEnter(GameObject character)
    {
        GameObject currentOccupant = getCurrentOccupant();
        if (currentOccupant == null || character == currentOccupant)
            return true;
        return false;
    }
}
